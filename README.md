# safety-analysis
A compositional, model-based framework for modeling, visualizing and analyzing the safety of architectures for critical systems. 

See doc/SafetyAnalysis.html and the rest of the files in the doc/ directory for detailed documentation.

The code is the tool/ directory.

See the file installation-instructions.txt for installation instructions.

A number of examples are in the tool/examples directory.

The initial release of this project is the result of a Cost Sharing type contract between NASA and GE.
